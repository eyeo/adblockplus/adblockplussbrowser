<?xml version="1.0" encoding="utf-8"?>
<!--
 This file is part of Adblock Plus <https://adblockplus.org/>,
 Copyright (C) 2006-present eyeo GmbH

 Adblock Plus is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 3 as
 published by the Free Software Foundation.

 Adblock Plus is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
  -->

<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:paddingStart="24dp"
        android:paddingEnd="24dp">

        <ImageView
            android:id="@+id/onboarding_welcome_block_ads_image"
            android:contentDescription="@string/onboarding_welcome_block_ads_title"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="32dp"
            android:src="@drawable/ic_onboarding_block_ads"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

        <TextView
            android:id="@+id/onboarding_welcome_block_ads_title"
            style="@style/OnboardingTitleTextStyle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="12dp"
            android:text="@string/onboarding_welcome_block_ads_title"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toEndOf="@id/onboarding_welcome_block_ads_image"
            app:layout_constraintTop_toTopOf="@id/onboarding_welcome_block_ads_image" />

        <TextView
            android:id="@+id/onboarding_welcome_block_ads_description"
            style="@style/OnboardingDescriptionTextStyle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginTop="4dp"
            android:text="@string/onboarding_welcome_block_ads_description"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="@id/onboarding_welcome_block_ads_title"
            app:layout_constraintTop_toBottomOf="@id/onboarding_welcome_block_ads_title" />

        <androidx.constraintlayout.widget.Barrier
            android:id="@+id/onboarding_welcome_block_ads_barrier"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:barrierDirection="bottom"
            app:constraint_referenced_ids="onboarding_welcome_block_ads_image, onboarding_welcome_block_ads_description" />

        <ImageView
            android:id="@+id/onboarding_welcome_tracking_protection_image"
            android:contentDescription="@string/onboarding_welcome_tracking_protection_title"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="24dp"
            android:src="@drawable/ic_onboarding_tracking_protection"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/onboarding_welcome_block_ads_barrier" />

        <TextView
            android:id="@+id/onboarding_welcome_tracking_protection_title"
            style="@style/OnboardingTitleTextStyle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="12dp"
            android:text="@string/onboarding_welcome_tracking_protection_title"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toEndOf="@id/onboarding_welcome_tracking_protection_image"
            app:layout_constraintTop_toTopOf="@id/onboarding_welcome_tracking_protection_image" />

        <TextView
            android:id="@+id/onboarding_welcome_tracking_protection_description"
            style="@style/OnboardingDescriptionTextStyle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginTop="4dp"
            android:text="@string/onboarding_welcome_tracking_protection_description"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="@id/onboarding_welcome_tracking_protection_title"
            app:layout_constraintTop_toBottomOf="@id/onboarding_welcome_tracking_protection_title" />

        <androidx.constraintlayout.widget.Barrier
            android:id="@+id/onboarding_welcome_tracking_protection_barrier"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:barrierDirection="bottom"
            app:constraint_referenced_ids="onboarding_welcome_tracking_protection_image, onboarding_welcome_tracking_protection_description" />

        <ImageView
            android:id="@+id/onboarding_welcome_security_privacy_image"
            android:contentDescription="@string/onboarding_welcome_security_privacy_title"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="24dp"
            android:src="@drawable/ic_onboarding_security_privacy"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/onboarding_welcome_tracking_protection_barrier" />

        <TextView
            android:id="@+id/onboarding_welcome_security_privacy_title"
            style="@style/OnboardingTitleTextStyle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="12dp"
            android:text="@string/onboarding_welcome_security_privacy_title"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toEndOf="@id/onboarding_welcome_security_privacy_image"
            app:layout_constraintTop_toTopOf="@id/onboarding_welcome_security_privacy_image" />

        <TextView
            android:id="@+id/onboarding_welcome_security_privacy_description"
            style="@style/OnboardingDescriptionTextStyle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginTop="4dp"
            android:text="@string/onboarding_welcome_security_privacy_description"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="@id/onboarding_welcome_security_privacy_title"
            app:layout_constraintTop_toBottomOf="@id/onboarding_welcome_security_privacy_title" />

        <androidx.constraintlayout.widget.Barrier
            android:id="@+id/onboarding_welcome_security_privacy_barrier"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:barrierDirection="bottom"
            app:constraint_referenced_ids="onboarding_welcome_security_privacy_image, onboarding_welcome_security_privacy_description" />

        <ImageView
            android:id="@+id/onboarding_welcome_language_filters_image"
            android:contentDescription="@string/onboarding_welcome_language_filters_title"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="24dp"
            android:src="@drawable/ic_onboarding_language_filters"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/onboarding_welcome_security_privacy_barrier" />

        <TextView
            android:id="@+id/onboarding_welcome_language_filters_title"
            style="@style/OnboardingTitleTextStyle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="12dp"
            android:text="@string/onboarding_welcome_language_filters_title"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toEndOf="@id/onboarding_welcome_language_filters_image"
            app:layout_constraintTop_toTopOf="@id/onboarding_welcome_language_filters_image" />

        <TextView
            android:id="@+id/onboarding_welcome_language_filters_description"
            style="@style/OnboardingDescriptionTextStyle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginTop="4dp"
            android:text="@string/onboarding_welcome_language_filters_description"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="@id/onboarding_welcome_language_filters_title"
            app:layout_constraintTop_toBottomOf="@id/onboarding_welcome_language_filters_title" />

    </androidx.constraintlayout.widget.ConstraintLayout>

</layout>