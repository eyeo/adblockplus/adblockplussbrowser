<!--
 This file is part of Adblock Plus <https://adblockplus.org/>,
 Copyright (C) 2006-present eyeo GmbH

 Adblock Plus is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 3 as
 published by the Free Software Foundation.

 Adblock Plus is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
  -->

<resources>
    <!-- app-->
    <string name="app_name">ABP pour Samsung Internet</string>
    <string name="app_subtitle">pour Samsung Internet</string>
    <string name="samsung_internet_required_title">Samsung Internet requis</string>
    <string name="samsung_internet_required_description">ABP est une application qui fonctionne en complément de Samsung Internet pour filtrer les publicités pendant que vous utilisez ce navigateur. Pour qu\'ABP soit efficace, vous devez installer Samsung Internet 4.0 ou une version ultérieure sur votre appareil.\nVous pouvez obtenir le navigateur Samsung Internet en appuyant sur le bouton ci-dessous.</string>
    <string name="download_samsung_internet">Télécharger Samsung Internet</string>
    <string name="update_status_progress_message">Récupération des mises à jour des listes de filtres…</string>
    <string name="update_status_error_message">Erreur de mise à jour : veuillez réessayer</string>
    <string name="update_status_retry">Réessayer</string>
    <string name="device_not_supported">Cet appareil n\'est pas compatible</string>

    <!-- base-->
    <string name="product_name">ABP</string>
    <string name="acceptable_ads">Publicité Acceptable</string>
    <string name="intrusive_ads">Publicités intrusives</string>
    <string name="acceptable_ads_fair_standard_title">Norme juste</string>
    <string name="acceptable_ads_fair_standard_description">Les publicités diffusées avec Publicité Acceptable ne sont jamais des publicités vidéo, sonores ou des pop-ups</string>
    <string name="acceptable_ads_placement_title">Placement</string>
    <string name="acceptable_ads_placement_description">Aucune perturbation majeure du flux de lecture naturel</string>
    <string name="acceptable_ads_distinction_title">Distinction</string>
    <string name="acceptable_ads_distinction_description">Les publicités diffusées avec Publicité Acceptable sont toujours clairement identifiées comme des publicités</string>
    <string name="acceptable_ads_size_title">Taille</string>
    <string name="acceptable_ads_size_description">Couvrent moins de 50% de l\'écran lors du chargement initial</string>
    <string name="acceptable_ads_standard_description">Voir une explication plus détaillée de la </string>
    <string name="acceptable_ads_standard_link">Norme relative aux annonces acceptables</string>

    <!-- onboarding -->
    <string name="onboarding_welcome_header_title1">Bienvenue à</string>
    <string name="onboarding_welcome_block_ads_title">Bloquez les publicités gênantes</string>
    <string name="onboarding_welcome_block_ads_description">Naviguez sans publicité tout en soutenant les créateurs de contenu à l\'aide de Publicité Acceptable</string>
    <string name="onboarding_welcome_tracking_protection_title">Protection de suivi</string>
    <string name="onboarding_welcome_tracking_protection_description">Naviguez en toute sécurité en empêchant les sites de vous suivre sur le Web</string>
    <string name="onboarding_welcome_security_privacy_title">Sécurité et confidentialité améliorées</string>
    <string name="onboarding_welcome_security_privacy_description">Naviguez sans souci avec ABP qui vous protège en arrière-plan</string>
    <string name="onboarding_welcome_language_filters_title">Listes de filtres spécifiques à la langue</string>
    <string name="onboarding_welcome_language_filters_description">Bloquez plus de publicités en optimisant ABP pour les langues dans lesquelles vous naviguez généralement</string>

    <string name="onboarding_acceptable_ads_header_title1">Participez au programme</string>
    <string name="onboarding_acceptable_ads_header_title3">et soutez les créateurs de contenu</string>
    <string name="onboarding_acceptable_ads_annoying_ads_title">Les publicités gênantes sont toujours bloquées</string>
    <string name="onboarding_acceptable_ads_annoying_ads_description">Les publicités gênantes qui vous empêchent de lire le contenu qui vous intéresse seront toujours bloquées</string>
    <string name="onboarding_acceptable_ads_nonintrusive_ads_title">Un petit nombre de publicités non intrusives sont affichées par défaut</string>
    <string name="onboarding_acceptable_ads_nonintrusive_ads_description">Des publicités qui n\'interfèrent pas avec votre expérience de navigation apparaîtront, afin que les créateurs puissent continuer à publier leur contenu</string>
    <string name="onboarding_acceptable_ads_nonintrusive_ads_example">Montrez-moi un exemple</string>
    <string name="onboarding_acceptable_ads_disable_title">Désactivez à tout moment</string>
    <string name="onboarding_acceptable_ads_disable_description">Vous pouvez modifier ce paramètre en appuyant sur l\'option \"Autoriser certaines publicités non-intrusives\".</string>

    <string name="onboarding_enable_header_title1">Voici comment</string>
    <string name="onboarding_enable_header_title2">Activer ABP</string>
    <string name="onboarding_enable_one_step_title">Une étape de plus et vous aurez terminé</string>
    <string name="onboarding_enable_one_step_description">Activer ABP dans la rubrique « Bloqueurs de publicité » des paramètres de Samsung Internet\</string>

    <string name="open_samsung_internet">Ouvrir Samsung Internet</string>

    <!-- /onboarding -->

    <!-- preferences -->
    <string name="preferences_ad_blocking_category">Options de blocage des publicités</string>
    <string name="preferences_primary_subscriptions_title">Langue</string>
    <string name="preferences_primary_subscriptions_summary">Bloquez plus de publicités en optimisant ABP pour la/les langue(s) dans laquelle/lesquelles vous naviguez généralement</string>
    <string name="preferences_other_subscriptions_title">Autres options de blocage</string>
    <string name="preferences_other_subscriptions_summary">Supprimer le suivi, les boutons de réseaux sociaux ou ajouter des listes de filtres personnalisées</string>
    <string name="preferences_allowlist_title">Ajouter des sites Web à la liste d\'autorisation</string>
    <string name="preferences_allowlist_summary">Désactiver le blocage des publicités sur les sites Web de votre choix</string>
    <string name="preferences_allowlist_header">Sites Web autorisés</string>
    <string name="preferences_updates_title">Mettre à jour les listes de filtres publicitaires</string>
    <string name="preferences_updates_subtitle">Définir quand mettre à jour les listes utilisées pour filtrer les publicités</string>
    <string name="preferences_automatic_updates_title">Mises à jour automatiques</string>
    <string name="preferences_automatic_updates_wifi_only">Wi-Fi uniquement</string>
    <string name="preferences_automatic_updates_always">Toujours</string>
    <string name="preferences_update_subscriptions_title">Mettre à jour maintenant</string>
    <string name="preferences_acceptable_ads_title">Autoriser certaines publicités non-intrusives</string>
    <string name="preferences_acceptable_ads_summary">Les publicités gênantes sont toujours bloquées, tandis que certaines publicités non intrusives sont affichées par défaut, afin que les créateurs de contenu puissent publier leur contenu gratuitement</string>
    <string name="preferences_acceptable_ads_action">Oui, je soutiens les créateurs de contenu</string>
    <string name="preferences_about_title">À propos</string>
    <string name="primary_subscriptions_active_category">Optimisé pour la navigation dans</string>
    <string name="primary_subscriptions_inactive_category">Langues supplémentaires</string>
    <string name="subscription_last_update">Dernière mise à jour : %s</string>
    <string name="subscription_last_update_never">Jamais</string>
    <string name="other_subscriptions_default_category">Listes de filtres intégrées</string>
    <string name="other_subscriptions_custom_category">Listes de filtres personnalisées actives</string>
    <string name="other_subscriptions_add_custom_title">Ajouter une liste de filtres personnalisée</string>
    <string name="other_subscriptions_add_custom_hint">www.example.com/filterlist.txt</string>
    <string name="file_manager_not_found_message">Veuillez installer un gestionnaire de fichiers</string>
    <string name="delete_dialog_title">Confirmer la suppression</string>
    <string name="other_subscriptions_remove_custom_message">Êtes-vous sûr de vouloir supprimer la liste de filtres personnalisée de %s ?</string>
    <string name="other_subscriptions_error_add_custom">Erreur lors de l\'ajout de la liste de filtres personnalisée</string>
    <string name="allowlist_add_title">Ajouter un site Web à la liste d\'autorisation</string>
    <string name="allowlist_add_hint">example.com</string>
    <string name="allowlist_delete_dialog_message">Êtes-vous sûr de vouloir supprimer %s de la liste d\'autorisation ?</string>
    <string name="acceptable_ads_enabled_line2">En maintenant Publicité Acceptable activée, vous aidez les créateurs de contenu à publier leur contenu gratuitement</string>
    <string name="acceptable_ads_disabled">Non, j\'aimerais désactiver Publicité Acceptable</string>
    <string name="allow_updates_on_mobile_data_connection">Autoriser les mises à jour sur la connexion de données mobiles</string>
    <string name="automatic_update_checkbox_hint">Les mises à jour automatiques utiliseront vos données mobiles \nlorsque vous n\'êtes pas connecté au wifi</string>
    <string name="about_privacy_policy"><a href="https://adblockplus.org/privacy">Politique de confidentialité</a></string>
    <string name="about_terms_of_use"><a href="https://adblockplus.org/terms">Conditions d\'utilisation</a></string>
    <string name="no_browser_found">Aucun navigateur détecté sur cet appareil</string>
    <string name="about_imprint">Mentions légales</string>
    <string name="open_source_licenses">Licences Open Source</string>
    <string name="version">Version</string>
    <string name="share_events_header">Contribuez à améliorer ABP</string>
    <string name="share_events_title">Partager des données anonymisées</string>
    <string name="share_events_description">Les données anonymisées nous aident à améliorer ABP, ce qui nous permet de vous offrir une meilleure expérience de navigation.</string>
    <string name="share_events_consent">Oui, j\'accepte de partager des données anonymisées.</string>
    <string name="languages_onboarding_title">Vous naviguez dans plusieurs langues ?</string>
    <string name="languages_onboarding_option_add">Ajouter une langue de blocage des publicités supplémentaire.</string>
    <string name="languages_onboarding_option_skip">Non merci, je ne navigue que dans une seule langue.</string>
    <string name="delete_swipe_hint">Pour supprimer une entrée, faites-la glisser vers la gauche.</string>
    <string name="file_picking_canceled">La cueillette de fichiers est annulée</string>

    <!-- image descriptions -->
    <string name="image_bullet">Symbole de puce</string>

    <!-- settings -->
    <string name="block_additional_tracking">Bloquer le suivi supplémentaire</string>
    <string name="block_social_media_tracking">Bloquer le suivi des icônes de réseaux sociaux</string>
    <!-- /settings -->

    <!-- Issue reporter -->
    <string name="issueReporter_page_title">Signalement de problèmes</string>
    <string name="issueReporter_page_description">Partagez les problèmes lorsqu\'ils surviennent.</string>
    <string name="issueReporter_typeSelector_heading">Quel type de problème rencontrez-vous ?</string>
    <string name="issueReporter_typeSelector_description">S\'il vous plaît sélectionnez un problème :</string>
    <string name="issueReporter_falsePositive_label">Le lien vers la page que j’essaie d\'afficher est brisé</string>
    <string name="issueReporter_falseNegative_label">Je vois encore des publicités</string>

    <string name="comment_title">Commentaire (facultatif) :</string>
    <string name="make_screenshot_of_samsung_internet">Faire une capture d\'écran de Samsung Internet</string>
    <string name="cancel">Annuler</string>
    <string name="report_issue_enter_email_hint">Entrez le courriel</string>
    <string name="report_issue_enter_url_hint">Entrer l\'URL</string>
    <string name="report_issue_enter_comment_hint">Entrez un commentaire</string>
    <string name="issueReporter_url_description"> Nous vous encourageons à entrer l\'URL où vous avez trouvé le problème. Pensez simplement à copier-coller le lien de la page.</string>

    <string name="issueReporter_email_description">Si vous saisissez votre adresse mail cela nous aidera. Nous pourrons alors vous contacter pour résoudre les problèmes difficiles comme le malware. N’oubliez pas qu’il n’est pas obligatoire de saisir une adresse e-mail et que votre adresse ne sera jamais partagée avec des tiers.</string>
    <string name="issueReporter_comment_description">Merci de bien vouloir nous aider à mieux comprendre le problème en laissant un commentaire ci-dessous.</string>
    <string name="issueReporter_anonymousSubmission_label">Soumettre anonymement</string>
    <string name="issueReporter_anonymousSubmission_warning">Nous ne serons pas en mesure de vous recontacter et votre signalement ne sera probablement pas traité prioritairement.</string>
    <string name="issueReporter_sendButton_label">Envoyer le signalement</string>
    <string name="issueReporter_email_label">E-mail :</string>
    <string name="issueReporter_privacyPolicy"><a href="https://adblockplus.org/en/privacy#issue-reporter">Politique de confidentialité</a></string>
    <string name="issueReporter_url_label">URL (facultatif) :</string>
    <string name="issueReporter_attached_image_thumbnail">vignette de l\'image jointe</string>
    <string name="tap_here_to_pick_screenshot">Appuyez ici pour choisir une capture d\'écran</string>
    <string name="processing_issue_report_screenshot_description">Chargement…</string>
    <string name="screenshot_reselect_description">Appuyez ici pour choisir une autre capture d\'écran</string>
    <string name="issueReporter_report_sent">Merci d\'avoir signalé le problème !</string>
    <string name="issueReporter_report_send_error">Impossible de traiter votre demande. Veuillez réessayer plus tard.</string>
    <string name="issueReporter_report_screenshot_too_large">Le fichier image est trop volumineux. Veuillez en choisir un autre.</string>
    <string name="issueReporter_report_screenshot_invalid">Le fichier de capture d\'écran n\'est pas valide. Veuillez en choisir un autre.</string>

    <!-- Start guide -->
    <string name="tour_header">Guide</string>
    <string name="take_tour_title">Faire un tour</string>
    <string name="take_tour_description">Apprenez à utiliser ABP pour une expérience de navigation plus agréable</string>
    <string name="tour_next_button_text">Suivant</string>
    <string name="tour_skip_button_text">Passer</string>
    <string name="tour_dialog_ad_blocking_options_text">Pour une expérience de navigation optimisée, pensez à ajuster ces paramètres</string>
    <string name="tour_add_languages">Ajoutez les langues dans lesquelles vous naviguez habituellement</string>
    <string name="tour_disable_social_media_tracking">Désactiver le suivi des réseaux sociaux lors de la navigation</string>
    <string name="tour_allowlist">Ajouter un site Internet à la liste d\'autorisation pour continuer à y voir des annonces</string>
    <string name="tour_last_step_description">C\'est ça ! Vous êtes prêt à utiliser ABP pour Samsung Internet au maximum</string>
    <string name="tour_last_step_button_done">Fait</string>

</resources>
