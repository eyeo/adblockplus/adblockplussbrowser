<!--
 This file is part of Adblock Plus <https://adblockplus.org/>,
 Copyright (C) 2006-present eyeo GmbH

 Adblock Plus is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 3 as
 published by the Free Software Foundation.

 Adblock Plus is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
  -->

<resources>
    <!-- app-->
    <string name="app_name">ABP for Samsung Internet</string>
    <string name="app_subtitle">for Samsung Internet</string>
    <string name="samsung_internet_required_title">Samsung Internet required</string>
    <string name="samsung_internet_required_description">ABP is an app that runs on top of Samsung Internet to filter ads while you use that browser. For ABP to be effective, you need Samsung Internet 4.0 or above installed on your device.\nYou can get Samsung Internet browser by tapping on the button below.</string>
    <string name="download_samsung_internet">Download Samsung Internet</string>
    <string name="update_status_progress_message">Fetching filter lists updates…</string>
    <string name="update_status_error_message">Error updating: Please try again</string>
    <string name="update_status_retry">Retry</string>
    <string name="device_not_supported">This device is not supported</string>

    <!-- base-->
    <string name="product_name">ABP</string>
    <string name="acceptable_ads">Acceptable Ads</string>
    <string name="intrusive_ads">Intrusive ads</string>
    <string name="acceptable_ads_fair_standard_title">Fair standard</string>
    <string name="acceptable_ads_fair_standard_description">Ads shown with Acceptable Ads are never video, sound or popup ads</string>
    <string name="acceptable_ads_placement_title">Placement</string>
    <string name="acceptable_ads_placement_description">No major disruption to the natural reading flow</string>
    <string name="acceptable_ads_distinction_title">Distinction</string>
    <string name="acceptable_ads_distinction_description">Ads shown with Acceptable Ads are always clearly marked as ads</string>
    <string name="acceptable_ads_size_title">Size</string>
    <string name="acceptable_ads_size_description">Span less than 50% of the screen upon initial load</string>
    <string name="acceptable_ads_standard_description">See a more detailed explanation of the </string>
    <string name="acceptable_ads_standard_link">Acceptable Ads Standard</string>

    <!-- onboarding -->
    <string name="onboarding_welcome_header_title1">Welcome to</string>
    <string name="onboarding_welcome_block_ads_title">Block annoying ads</string>
    <string name="onboarding_welcome_block_ads_description">Browse ad-free while supporting content creators through Acceptable Ads</string>
    <string name="onboarding_welcome_tracking_protection_title">Tracking protection</string>
    <string name="onboarding_welcome_tracking_protection_description">Browse safer by preventing websites from tracking you across the web</string>
    <string name="onboarding_welcome_security_privacy_title">Improved security &amp; privacy</string>
    <string name="onboarding_welcome_security_privacy_description">Browse worry-free with ABP protecting you in the background</string>
    <string name="onboarding_welcome_language_filters_title">Language-specific filter lists</string>
    <string name="onboarding_welcome_language_filters_description">Block more ads by optimizing ABP for the languages you typically browse in</string>

    <string name="onboarding_acceptable_ads_header_title1">Participate in</string>
    <string name="onboarding_acceptable_ads_header_title3">and support content creators</string>
    <string name="onboarding_acceptable_ads_annoying_ads_title">Annoying ads are always blocked</string>
    <string name="onboarding_acceptable_ads_annoying_ads_description">Annoying ads, distracting you from the content you love will always be blocked</string>
    <string name="onboarding_acceptable_ads_nonintrusive_ads_title">A small number of nonintrusive ads are displayed by default</string>
    <string name="onboarding_acceptable_ads_nonintrusive_ads_description">Ads that don\'t interfere with your browsing experience will appear, so that creators can continue publishing their content</string>
    <string name="onboarding_acceptable_ads_nonintrusive_ads_example">Show me an example</string>
    <string name="onboarding_acceptable_ads_disable_title">Disable anytime</string>
    <string name="onboarding_acceptable_ads_disable_description">You can change this setting by tapping the \"Allow some nonintrusive advertising\" option</string>

    <string name="onboarding_enable_header_title1">Here\'s how to</string>
    <string name="onboarding_enable_header_title2">Enable ABP</string>
    <string name="onboarding_enable_one_step_title">One more step and you\'re done</string>
    <string name="onboarding_enable_one_step_description">Enable ABP in Samsung Internet\'s Adblocker Settings</string>

    <string name="open_samsung_internet">Open Samsung Internet</string>
    <!-- /onboarding -->

    <!-- preferences -->
    <string name="preferences_ad_blocking_category">Ad blocking options</string>
    <string name="preferences_primary_subscriptions_title">Language</string>
    <string name="preferences_primary_subscriptions_summary">Block more ads by optimizing ABP for the language(s) you typically browse in</string>
    <string name="preferences_other_subscriptions_title">More blocking options</string>
    <string name="preferences_other_subscriptions_summary">Remove tracking, social media buttons or add custom filter lists</string>
    <string name="preferences_allowlist_title">Add websites to the allowlist</string>
    <string name="preferences_allowlist_summary">Disable ad blocking on websites of your choosing</string>
    <string name="preferences_allowlist_header">Allowlisted websites</string>
    <string name="preferences_updates_title">Update ad-filter lists</string>
    <string name="preferences_updates_subtitle">Configure when to update the lists used to filter ads</string>
    <string name="preferences_automatic_updates_title">Automatic updates</string>
    <string name="preferences_automatic_updates_wifi_only">Wi-Fi only</string>
    <string name="preferences_automatic_updates_always">Always</string>
    <string name="preferences_update_subscriptions_title">Update now</string>
    <string name="preferences_acceptable_ads_title">Allow some nonintrusive advertising</string>
    <string name="preferences_acceptable_ads_summary">Annoying ads are always blocked, while some nonintrusive ads are displayed by default, so that content creators can publish their content for free</string>
    <string name="preferences_acceptable_ads_action">Yes, I\'ll support content creators</string>
    <string name="preferences_about_title">About</string>
    <string name="primary_subscriptions_active_category">Optimized for browsing in</string>
    <string name="primary_subscriptions_inactive_category">Additional languages</string>
    <string name="subscription_last_update">Last update: %s</string>
    <string name="subscription_last_update_never">Never</string>
    <string name="other_subscriptions_default_category">Built-in filter lists</string>
    <string name="other_subscriptions_custom_category">Active custom filter lists</string>
    <string name="other_subscriptions_add_custom_title">Add custom filter list</string>
    <string name="other_subscriptions_add_custom_hint">www.example.com/filterlist.txt</string>
    <string name="file_manager_not_found_message">Please, install a file manager</string>
    <string name="delete_dialog_title">Confirm deletion</string>
    <string name="other_subscriptions_remove_custom_message">Are you sure you want to delete the custom filter list from %s?</string>
    <string name="other_subscriptions_error_add_custom">Error adding custom filter list</string>
    <string name="allowlist_add_title">Add a website to the allowlist</string>
    <string name="allowlist_add_hint">example.com</string>
    <string name="allowlist_delete_dialog_message">Are you sure you want to remove %s from the allowlist?</string>
    <string name="acceptable_ads_enabled_line2">By keeping Acceptable Ads enabled, you\'re helping content creators publish their content for free</string>
    <string name="acceptable_ads_disabled">No, I\'d like to disable Acceptable Ads</string>
    <string name="allow_updates_on_mobile_data_connection">Allow updates on mobile data connection</string>
    <string name="automatic_update_checkbox_hint">Automatic updates will use your mobile data \nwhen you are not connected to wifi</string>
    <string name="about_privacy_policy"><a href="https://adblockplus.org/privacy">Privacy Policy</a></string>
    <string name="about_terms_of_use"><a href="https://adblockplus.org/terms">Terms of Use</a></string>
    <string name="no_browser_found">No browser found on this device</string>
    <string name="about_imprint">Imprint</string>
    <string name="open_source_licenses">Open Source Licenses</string>
    <string name="version">Version</string>
    <string name="share_events_header">Help improve ABP</string>
    <string name="share_events_title">Share anonymized data</string>
    <string name="share_events_description">Anonymized data helps us improve ABP, thus creating a better browsing experience for you.</string>
    <string name="share_events_consent">Yes, I\'ll share some anonymized data.</string>
    <string name="languages_onboarding_title">Browsing in multiple languages?</string>
    <string name="languages_onboarding_option_add">Add an additional ad blocking language.</string>
    <string name="languages_onboarding_option_skip">No thanks, I only browse in one language.</string>
    <string name="delete_swipe_hint">To delete an entry, swipe it to the left.</string>
    <string name="file_picking_canceled">File picking is canceled</string>

    <!-- image descriptions -->
    <string name="image_bullet">Bullet Point Symbol</string>

    <!-- settings -->
    <string name="block_additional_tracking">Block additional tracking</string>
    <string name="block_social_media_tracking">Block social media icons tracking</string>
    <!-- /settings -->

    <!-- Issue reporter -->
    <string name="issueReporter_page_title">Issue reporter</string>
    <string name="issueReporter_page_description">Share issues when they happen.</string>
    <string name="issueReporter_typeSelector_heading">Select issue type</string>
    <string name="issueReporter_typeSelector_description">This window will guide you through the steps required for the
        submission of an Adblock Plus issue report. First, please select the type of issue that you are experiencing on
        this page:
    </string>
    <string name="issueReporter_falsePositive_label">Adblock Plus is blocking too much</string>
    <string name="issueReporter_falseNegative_label">Adblock Plus doesn\'t block an advertisement</string>

    <string name="comment_title">Comment (optional):</string>
    <string name="make_screenshot_of_samsung_internet">Make screenshot of Samsung Internet</string>
    <string name="cancel">Cancel</string>
    <string name="report_issue_enter_email_hint">Enter email</string>
    <string name="report_issue_enter_url_hint">Enter URL</string>
    <string name="report_issue_enter_comment_hint">Enter comment</string>

    <string name="issueReporter_email_description">We encourage you to enter a valid email address so that we can contact you if
        there are questions about your report. It will also allow us to recognize your contributions and to prioritize
        them higher.
    </string>
    <string name="issueReporter_url_description"> We encourage you to enter the URL where you found
        the issue. Just think of copying and pasting the link of the page.
    </string>
    <string name="issueReporter_comment_description">The text field below allows you to enter a comment to help us understand the
        issue. This step is optional but recommended if the problem isn\'t obvious. You can also review the report data
        before it is sent.
    </string>
    <string name="issueReporter_anonymousSubmission_label">Anonymous submission</string>
    <string name="issueReporter_anonymousSubmission_warning">We won\'t be able to come back to you and will likely prioritize the
        report lower.
    </string>
    <string name="issueReporter_sendButton_label">Send Report</string>
    <string name="issueReporter_email_label">Email:</string>
    <string name="issueReporter_url_label">URL (optional):</string>
    <string name="issueReporter_attached_image_thumbnail">attached image thumbnail</string>
    <string name="issueReporter_privacyPolicy"><a href="https://adblockplus.org/en/privacy#issue-reporter">Privacy Policy</a></string>
    <string name="tap_here_to_pick_screenshot">Tap here to pick a screenshot</string>
    <string name="processing_issue_report_screenshot_description">Loading…</string>
    <string name="screenshot_reselect_description">Tap here to pick a different screenshot</string>
    <string name="issueReporter_report_sent">Thanks for reporting the issue!</string>
    <string name="issueReporter_report_send_error">"Unable to process your request. Please try again later."</string>
    <string name="issueReporter_report_screenshot_too_large">The image file is too large. Please pick another one.</string>
    <string name="issueReporter_report_screenshot_invalid">The screenshot file is invalid. Please pick another one.</string>

    <!-- Start guide -->
    <string name="tour_header">Guide</string>
    <string name="take_tour_title">Take a tour</string>
    <string name="take_tour_description">Learn how to use ABP for a more enjoyable browsing experience</string>
    <string name="tour_next_button_text">Next</string>
    <string name="tour_skip_button_text">Skip it</string>
    <string name="tour_dialog_ad_blocking_options_text">For optimized browsing experience,\nthink of adjusting these parameters</string>
    <string name="tour_add_languages">Add the languages you usually browse in</string>
    <string name="tour_disable_social_media_tracking">Disable social media tracking while browsing</string>
    <string name="tour_allowlist">Allowlist a website to continue seeing ads on it</string>
    <string name="tour_last_step_description">That’s it! You are ready to use ABP for Samsung Internet to its fullest</string>
    <string name="tour_last_step_button_done">Done</string>

</resources>
