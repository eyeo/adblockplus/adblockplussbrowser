/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.adblockplus.adblockplussbrowser.base.yandex

object YandexConstants {
    const val YANDEX_APP_NAME = "yandex"
    const val YANDEX_PACKAGE_NAME = "com.yandex.browser"
    const val YANDEX_ALPHA_PACKAGE_NAME = "com.yandex.browser.alpha"
    const val YANDEX_BETA_PACKAGE_NAME = "com.yandex.browser.beta"
}
